## Explanation of Approach

### File organization

This repository contains a solution to the Parenting API integration exercise.

This approach uses Python 3 with the Django framework. Please refer to the 
README.md file at the root of the project to see how to bootstrap and test 
the project locally.

Many of the files contained in this repo are simply for scaffolding, but please
refer to these three files to see the crux of the solution:

- *parenting/post/models.py* - The model file contains the data model schema
shared by external and internal posts.
- *parenting/post/api.py* - The api file contains the API configuration and
views.
- *parenting/post/management/commands/sync_faker_posts.py* - This file
contains the logic for retrieving and syncing files from the external API

I have include many comments within the code to indicate my thought process,
including some TODO markers where more exploration would be appropriate.

### Considerations

#### Relying on a third-party provider for content

Integrating with a third-party data source, while quite common, is also full of 
potential pitfalls:

- The API can become unavailable (due to outage, invalid API credentials, etc)
- The API can be slow
- The API schema can change (both in the structure and assumptions about the data)
- The API data may need to be cleaned and transformed

With these pitfalls in mind, I wanted to use an approach that would be 
resilient in the case of any API turbulence. 

The first thing I wanted to make sure of is that we're not trying to retrieve
any of this data live, through our API. If we took an approach like that, our
API would have no defenses against outages or issues with the external API.
Instead, we can cache the external posts to the same object store that we're 
using for CMS posts and reduce our points of failure.

In this solution I've created a command line script that can be scheduled 
as frequently as needed through something like a CRON job:
```
	python manage.py sync_faker_posts
```

#### Unifying multiple data sources

My understanding is that we are integrating posts from two sources: some
posts are coming directly from this CMS, while others are coming from the 
external FakerRQ API. In this approach I'm using a database as the object 
store of choice for posts from both sources.

From the outset I wanted to unify these two schemas for two reasons:
First it's much easier to work from a single schema; and second because we are 
using a database, and having posts in a single table will greatly reduce 
database queries.

Within the schema, we are keeping track of the original source of the post
content (CMS or FakerRQ), the external FakerRQ ID if relevant, and the date
it was last synchronized. Keeping a record of the external ID can be critical
if we need to synchronize any updates that may have been made to a post after 
it was originally published.

##### Syncing Authors

Not knowing about the nature of the content, I am assuming that there's a 
possibility that an author may be added directly to the CMS, and then later
appear in the external API feed. With that in mind, extra logic is taken
in to account to identify and synchronize authors across different sources.
(As implemented, there is a possibility that two authors with the same first
and last names would be identified as the same person. A more robust solution
would use something more unique - such as an email address - to uniquely
identify an author.)

##### Syncing Avatar Assets

As mentioned above, we want to be as defensive as possible with the external
API. Any change in the media format could have big implications on the front
end speed and display. To neutralize that, I am copying over the author's
avatar to the CMS where it can be treated or resized to match the rest
of the author avatars.

In this exercise I'm not implementing any sort of image thumbnailing, but
in production it would be best to provide a variety of thumbnail sizes to the 
API user.

##### Standardizing Text Formats

The body text format wasn't brought up in the exercise prompt, but more 
consideration would be needed to ensure that the text is cleaned and 
standardized to be used in the CMS. Some text variations that would need to be 
considered: 

- Does the API standardize the text encoding? How does it handle special 
characters?
- Does the text markup method (Plain? HTML? Markdown?) differ from the CMS?
- Is there style embdded markup, which is often added haphazardly though WYSIWYG 
editors?
- Is there embedded content such as images, iframes or scripts; and do we need 
to remove or sanitize it?

#### Unprotected API Endpoint

I am assuming that this is a completely open and public API endpoint as it 
wasn't noted otherwise in the exercise.

Without any sort of authentication or rate limiting, there could be an unknown 
number of users making an unknown number of requests to it. As such, it's 
especially important that the API can perform well in high traffic conditions.

In this solution I have implemented a minimal amount of caching to reduce
the database queries and view rendering, but in a production environment
I would recommend more aggressive caching and request rate limiting outside of 
the application layer.

#### API Health

In this exercise I am assuming that the API Health endpoint is merely an 
uptime indicator to identify if the application is running or not.

In production, I could imagine this extending to include other metrics that
provide more insight, such as the uptime of the external API.

-------

## Infrastructure, Deployment, and Hosting

### Infrastructure

This solution is a python-based web application that uses a database, caching, 
and file storage:

- Linux Server with the following isntalled:
	- UWSGI serving the python application through a local socket
	- File storage either on disk, or a managed file storage service such as S3
	- NGINX for serving the UWSGI socket and local file storage
	- Memcached/Varnish for application caching
	- Postgres/MySQL server either on the server, or as managed service

### Deployment

I recommend running at minimum a separate dev and production environment with
a continuous integration service to manage deployments.

This could be configured such that commits to the master and dev branches
of the repository trigger new builds on the production and dev servers 
respectively. After a successful deployment, the CI service could run a 
checklist of tests to ensure the server continues to perform correctly with 
the latest updates, or notify the team if not. If a test fails, the server
could either roll back or notify a developer to manually roll back.

Django-based applications use a relatively simple deployment procedure: 
- Code is retrieved from repository
- Required libraries are installed or updated through PIP
- The database schema is updated
- UWSGI is restarted

A configuration management system like Salt or Chef would be helpful to manage
the configuration of the server and backing services as a whole.

### Hosting 

This system could be hosted on any host that provides a private or pseudo-private
server, or on a managed application service.

As the application currently stands, I would recommend about 4GB of RAM on the 
server. Because so much of the application logic can simply be cached, the 
application's memory requirements are quite low; we simply need enough RAM to 
run the backing services as well (database, caching).

### Monitoring

In order to monitor the health of the application, I would recommend monitoring 
the following:

- API Uptime to identify if API endpoint is down.
- Server file system capacity (particularly if serving files locally) to 
identify if the disk is at risk of filling up and locking.
- Server and database memory to identify if the system is at risk of being 
overtaxed.
- Application logs to identify if unexpected and unhandled errors are 
happening within the application.

-------

## Best Practices

As I've worked on more and more projects (and made mistakes along the way!), 
I've been building a best practices rubric that I wanted to share so you can 
get a feel for my overall approach.

As my work has shifted from developing isolated code to designing and 
maintaining systems, I've found that there are some key indicators that make
an application a good citizen within their environments.

I've included this partial list as a sort of appendix to the exercise to help 
illustrate some of the underlying concerns I have when developing an 
application. I would love to talk more about these and hear what's on your 
best practice checklist!

### Maintainability: Any developer can open a project and bootstrap it easily, because:
- Application README contains all the information to get up and running for development.
- Application dependencies are maintained with specific version numbers.
- Application provides data fixtures or mock APIs if necessary to run the software.
- Application provides documentation for building and deploying production software.
- Application can be built and deployed automatically without human intervention.

### Defensive: It would be hard to break this application, because:
- Application runtime dependencies are carefully identified and managed. Assuming all external services will fail at some point - even intermittently - or that the user's internet will fail - the app has graceful or non-graceful fallback strategies in place. [In a way, a web application is mostly dependencies: Fonts, Styles, Scripts, Data, Communication Sockets, 3rds Party Blocking Tools (apis, captcha), 3rd Party Passive Tools (maps, analytics), Databases]
- Application reduces risk by validating and caching external data before using it.
- Critical failures exit the Application with a fatal error.
- Application and backing services are monitored.
- If the application has a GUI, it provides clear visual progress and status of loading and fatal errors.

### Debuggable: If something is wrong with the application, I know immediately and have tools to remediate it because:
- Application catches errors and reports them to the designated logging location.
- Someone is notified when an unexpected error happens in the application.
- Application only catches specific errors it expects and doesn't hide unexpected errors with a global catch.
- If anything depends on this app, it sends heartbeats somewhere so that it can be determined if the app is running or not.
- Application logs to a log file or server asynchronously, it doesn't pollute log files with redundant data, the log file contains a date stamp and log level, and these log files are rotated so they don't fill up a drive.
- Application has a debug mode that can be turned on through external parameters.
- The application builds are versioned so I can log errors against specific versions.

### Flexible:
- All variables such as URLs, Media Roots, Static Roots, Usernames, Password, etc are passed in to the application via command line arguments or a config file. I don't need to rebuild the application to update these settings.
- Application can run anywhere it is placed, relying only on relative paths or paths specified by environment variables.
- Application can be run on any platform.
- Application can run with a slow or unreliable network connection.

### Economical: 
- Application doesn't waste network resources with re-downloading media it could cache.
- Application deallocates and resources it allocates when done with them.
- Application doesn't waste network resources with unnecessarily large payloads.
- Application doesn't waste network resources with unnecessary chatter.
- Application doesn't waste energy resources with inefficient programming and unnecessary processing cycles.
- Application doesn't waste hard drive space by including or downloading unnecessary files.

### Secure:
- If the application source code was ever exposed, it would not contain any comprimising information or credentials.
- Sensitive data (passwords, email addresses, PII, etc) printed in log files or transmitted are masked so that most or all of the characters and length are hidden, e.g. Passw0rd > Pass****************************** 
- Assuming all credentials stored on client computers can be comprimised, they should provide the least amount of privileges. e.g. Be read-only if possible
- Debugging modes that expose extra data or credentials are never deployed to production; dev builds never have access to production credentials.

