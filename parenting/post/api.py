from collections import OrderedDict

from django.conf import settings
from django.conf.urls import url, include
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page

from rest_framework import routers, serializers, viewsets, filters, generics
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.response import Response

from django_filters.rest_framework import DjangoFilterBackend

from .models import Author, Post


class AuthorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Author
        fields = ('id', 'firstName', 'lastName')


class PostSerializer(serializers.HyperlinkedModelSerializer):

    author = AuthorSerializer(many=False)

    # Use custom date formatter: Mon Sep 24 2018 23:29:24 GMT+0000 (UTC)
    createdAt = serializers.DateTimeField(
        format='%a %b %d %Y %H:%M:%S GMT%z (%Z)') 

    class Meta:
        model = Post
        fields = ('id', 'title', 'author', 'createdAt')

class PostPagination(LimitOffsetPagination):
    # Customize default limit offset pagination to match desired schema
    
    default_limit = 25
    limit_query_param = 'max'

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            # Pagination helpers were not explicitely requested, 
            # so I'll remove them to match the desired schema more strictly
            # ('count', self.count),
            # ('next', self.get_next_link()),
            # ('previous', self.get_previous_link()),
            ('posts', data)
        ]))

class PostViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = Post.objects.published()
    serializer_class = PostSerializer

    pagination_class = PostPagination

    # Normally using the library's "ordering_fields" functionality is preferred
    # but to match the desired params, we implement this manually
    # ordering_fields = ('createdAt',)
    ORDERING_DESC = 'desc'
    ORDERING_ASC = 'asc'

    def get_queryset(self):

        # Prefetch any foreign key relationships to reduce database query to 1
        queryset = Post.objects.published().select_related('author')

        ordering = self.request.GET.get('order', PostViewSet.ORDERING_DESC)
        if ordering == PostViewSet.ORDERING_ASC:
            queryset = queryset.order_by('createdAt')
        else:
            queryset = queryset.order_by('-createdAt')

        return queryset

    # Additional caching at the server level with something like Varnish is
    # recommended. With the caching below we are still hitting the database
    # twice, which is not needed for a public unprotected API endpoint
    @method_decorator(cache_page(settings.API_CACHE_DURATION))
    def list(self, request, *args, **kwargs):
        # Override default list schema so that it is nested in a "posts" object

        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        
        return Response({"posts":serializer.data})


"""
Note it might be better to place this in a separate monitoring type app
but for clarity of this exercise I'm including it with the other endpoints
"""

class APIHealthView(viewsets.ReadOnlyModelViewSet):

    def get_queryset(self):
        # TODO -- determine what should the API Health endpoint should
        # actually be representing
        return Post.objects.none()

    def list(self, request, *args, **kwargs):
        return Response({'Status': 'OK'})


