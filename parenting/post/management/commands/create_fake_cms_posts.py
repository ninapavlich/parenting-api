import random
from datetime import datetime, timedelta

from django.core.management.base import BaseCommand, CommandError

from parenting.post.models import Post, Author

import lorem

class Command(BaseCommand):
    help = 'Create fake CMS posts'

    def add_arguments(self, parser):
        parser.add_argument('count', nargs='+', type=int)
        parser.add_argument('--dryrun', action='store_true', default=False)

    def handle(self, *args, **options):

        ctr = 0
        dryrun = options['dryrun']
        count =  max(int(options['count'][0]), 0)
        while ctr < count:
            ctr += 1
            p = Post(
                title=lorem.sentence()[:-1],
                body=lorem.text(),
                createdAt=random_date(),
                author=Author.objects.order_by('?').first(),
                published=random.choice([True, True, False])
            )

            message = u"New post title: %s author: %s createdAt: %s"%(p.title, p.author, p.createdAt)
            if dryrun:
                message = u"[DRY RUN] %s"%(message)
            print(message)
            if not dryrun:
                p.save()

def random_date():

    start = datetime.strptime('1/1/2018 12:00 AM', '%m/%d/%Y %I:%M %p')
    end = datetime.strptime('1/1/2020 12:00 AM', '%m/%d/%Y %I:%M %p')
    
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = random.randrange(int_delta)
    return start + timedelta(seconds=random_second)        