import random
import json
import string
from datetime import datetime, timedelta

from django.core.management.base import BaseCommand, CommandError

from parenting.post.models import Post, Author

import lorem

class Command(BaseCommand):
    help = 'Create fake CMS posts'

    def add_arguments(self, parser):
        parser.add_argument('count', nargs='+', type=int)

    def handle(self, *args, **options):

        ctr = 0
        count =  max(int(options['count'][0]), 0)

        posts = []

        """
        Match output schema:
        {
          allPosts(count: 5) {
            id
            title
            body
            published
            createdAt
            author {
              id
              firstName
              lastName
              avatar
            }
          }
        }
        """

        fake_authors = get_fake_authors()
        
        while ctr < count:
            ctr += 1
            post = {
                'id':random_id(),
                'title':lorem.sentence()[:-1],
                'body':lorem.text(),
                'published':random.choice([True, True, False]),
                'createdAt':random_date().strftime('%a %b %d %Y %H:%M:%S GMT%z (%Z)'),
                'author': random.choice(fake_authors)
            }
            posts.append(post)


        output = {
            "data": {
                "allPosts":posts
            }
        }
        print( json.dumps(output, indent=4) )



def random_id():
    length = 6
    return  ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(length))

def get_fake_authors(): 

    fake_authors = [ 
        {
            'id':random_id(),
            'firstName':'Millard',
            'lastName':'Ickes',
            'avatar':'https://www.fillmurray.com/g/400/600'
        },
        {
            'id':random_id(),
            'firstName':'Mariette',
            'lastName':'Bongiorno',
            'avatar':'https://www.fillmurray.com/500/700'
        },
        {
            'id':random_id(),
            'firstName':'Lorene',
            'lastName':'Ahern',
            'avatar':'https://www.fillmurray.com/600/400'
        },
        {
            'id':random_id(),
            'firstName':'Han',
            'lastName':'Colosimo',
            'avatar':'https://www.fillmurray.com/500/600'
        },
        {
            'id':random_id(),
            'firstName':'Damaris',
            'lastName':'Disla',
            'avatar':'https://www.fillmurray.com/700/500'
        }
    ]

    # Also add the existing authors in there, so we can test synchronizing 
    # authors who already exist in the CMS
    all_authors = Author.objects.all()[:3]
    for author in all_authors:
        fake_authors.append({
            'id':random_id(),
            'firstName':author.firstName,
            'lastName':author.lastName,
            'avatar':'https://www.fillmurray.com/500/500'
        })

    return fake_authors



def random_date():

    start = datetime.strptime('1/1/2018 12:00 AM GMT+0000 (UTC)', '%m/%d/%Y %I:%M %p GMT%z (%Z)')
    end = datetime.strptime('1/1/2020 12:00 AM GMT+0000 (UTC)', '%m/%d/%Y %I:%M %p GMT%z (%Z)')
    
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = random.randrange(int_delta)
    return start + timedelta(seconds=random_second)  