import os

from django.db import models
from django.db.models.signals import pre_save, pre_delete
from django.dispatch import receiver
from django.utils.text import slugify



class ExternalSourceMixin(models.Model):
    """A base class to manage metadata related to object from external sources

    Attributes:
        extSource       The original source of the object
        extID           The objects ID from the external source
        extSyncDate     The date the object was last synced
    """

    POST_SOURCE_CMS = 'cms'
    POST_SOURCE_FAKERQL = 'fakerql'
    POST_SOURCES = (
        (POST_SOURCE_CMS, 'CMS'),
        (POST_SOURCE_FAKERQL, 'FakerQL'),
    )
    extSource = models.CharField(max_length=16, choices=POST_SOURCES, 
        default=POST_SOURCE_CMS, verbose_name="External Source")
    extID = models.CharField(max_length=255, null=True, blank=True, 
        verbose_name="External ID")
    extSyncDate = models.DateTimeField(null=True, blank=True, 
        verbose_name="External Sync Date")


    class Meta:
        abstract = True


def author_avatar_uploads(instance, original_filename):

    # Make sure avatar file is upload somewhere organized and URL-friendly
    # (In our case unique is being enforced by default file handler)
    cleaned_filename = '%s.%s'%(
        slugify( original_filename.split('.')[0].lower() ),
        original_filename.split('.')[1].lower()
    )
    return os.path.join(
        instance._meta.app_label.lower(), 
        instance._meta.object_name.lower(), 
        'avatars', 
        cleaned_filename
    )

class Author(ExternalSourceMixin):
    
    firstName = models.CharField(max_length=255, verbose_name="First Name")
    lastName = models.CharField(max_length=255, verbose_name="Last Name")

    # Probably would be better as an FK to a common media storage model
    avatar = models.FileField(upload_to=author_avatar_uploads, 
        blank=True, null=True)
    extAvatarURL = models.CharField(max_length=255, null=True, blank=True, 
        verbose_name="External Avatar URL")

    def get_full_name(self):
        return ' '.join(filter(None, [self.firstName, self.lastName]))


    def delete_avatar(self):

        # Deleting remote storage may require more complex logic
        if self.avatar and self.avatar.path:
            self._meta.get_field('avatar').storage.delete(self.avatar.path)


    def save(self, *args, **kwargs):

        # Delete the old avatar file if it has changed:
        if self.pk:
            old_instance = Author.objects.get(pk=self.pk)
            if old_instance.avatar != self.avatar:
                old_instance.delete_avatar()

        super(Author, self).save(*args, **kwargs)

    def __str__(self):
        return u'%s (%s)'%(self.get_full_name(), self.extSource)

    class Meta:
        ordering = ['lastName', 'firstName']
    

class PostQuerySet(models.QuerySet):

    def published(self):
        return self.filter(published=True)


class PostManager(models.Manager):

    def get_queryset(self):
        return PostQuerySet(self.model, using=self._db)

    def published(self):
        return self.get_queryset().published()


class Post(ExternalSourceMixin):

    objects = PostManager()

    title = models.CharField(max_length=255)
    body = models.TextField()
    published = models.BooleanField(default=False)

    createdAt = models.DateTimeField()
    author = models.ForeignKey('post.Author', on_delete=models.SET_NULL, 
        blank=True, null=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-createdAt']




"""
NOTE -- By convention, this would go in a signals.py file, but for 
this exercise I'm leaving the signal code in the models class for simplicity
"""

@receiver(pre_delete, sender=Author)
def delete_logfile_signal(sender, instance, **kwargs):
    
    # Make sure to clean up files on disk when we're done with them:
    if instance.avatar:
        instance.delete_avatar()