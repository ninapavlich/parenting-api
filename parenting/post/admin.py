from django import forms
from django.contrib import admin
from django.db.models import Q
from django.utils.html import format_html

from ckeditor.widgets import CKEditorWidget
from dal import autocomplete

from .models import Author, Post


@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):

    def avatar_thumbnail(self, obj):
        if obj.avatar:
            return format_html('<img src="%s" style="width: 125px;"/>'%(obj.avatar.url))
        return ''
    
    list_display = ['avatar_thumbnail', 'firstName', 'lastName', 'extID']
    readonly_fields = ['extAvatarURL', 'extSource', 'extID', 'extSyncDate', 'avatar_thumbnail']
    fieldsets = (
        (None, {
            'fields': (
                'firstName',
                'lastName',
                'avatar'
            )
        }),
        ('External Metadata', {
            'fields': (
                'extSource', 
                'extID', 
                'extAvatarURL',
                'extSyncDate'
            ),
        }),
    )





class AuthorAdminAutocomplete(autocomplete.Select2QuerySetView):
    # NOTE - a more generic approach would be better here
    def get_queryset(self):
        
        if not self.request.user.is_authenticated:
            return Author.objects.none()
        if not self.request.user.is_staff:
            return Author.objects.none()

        if not self.request.user.has_perm('post.view_author'):
            return Author.objects.none()

        qs = Author.objects.all()

        if self.q:
            qs = qs.filter(Q(firstName__icontains=self.q)
                |Q(lastName__icontains=self.q))

        return qs


class PostAdminForm(forms.ModelForm):
    body = forms.CharField(widget=CKEditorWidget())


    class Meta:
        model = Post
        fields = ('__all__')
        js = ('admin/autocomplete/forward.js',
              'admin/autocomplete/select_admin_autocomplete.js')

        widgets = {
            'author': autocomplete.ModelSelect2(
                url='author-autocomplete',
                attrs={'data-html': True}
            )
        }

@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ['title', 'createdAt', 'author', 'published', 'extSource']    
    list_filter = ['createdAt', 'author', 'published', 'extSource']
    readonly_fields = ['createdAt', 'extSource', 'extID', 'extSyncDate']
    form = PostAdminForm

    fieldsets = (
        (None, {
            'fields': (
                ('createdAt','published',),
                'title',
                'body',
                'author'
            )
        }),
        ('External Metadata', {
            'fields': (
                'extSource', 
                'extID', 
                'extSyncDate'
            ),
        }),
    )

