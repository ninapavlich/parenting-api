# Parenting API Exercise


## Local Development Quickstart

### Pre-Requisites:

* Python 3
* PIP
* Virtualenv

Run the following commands in your terminal:

```bash
	git clone git@bitbucket.org:ninapavlich/parenting-api.git
    cd parenting-api
```

Copy the contents of env.example into a file called .env at the root of the 
project directory. Update the SECRET_KEY value with some other random string.

Then run the following commands

```bash
    virtualenv venv
    source venv/bin/activate
    pip install -r requirements.txt
    python manage.py migrate
    python manage.py loaddata parenting/post/fixtures/example_data.json
    python manage.py runserver localhost:9999

    # To run ssl server:
    python manage.py runsslserver localhost:9999
```

Note: If you use the local SSL server, the certificate is self-signed
and you will receive a warning in your browser that you will need to accept.

With the local server running, you may browse the admin CMS at:

http://localhost:9999/admin/
Username: admin
Password: admin



### Development Tools

#### Sync with FakerQL API
```
    python manage.py sync_faker_posts
```

#### Generate 5 example posts that are directly inserted into the CMS:
```
    python manage.py create_fake_cms_posts 5
```

#### Generate 5 example posts for graphql testing:
```
    python manage.py create_fake_external_posts 5
```


#### Update Example Data Fixture
```
    python manage.py dumpdata --natural-foreign --indent=4 > parenting/post/fixtures/example_data.json
```
